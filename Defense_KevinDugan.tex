\documentclass[presentation]{beamer}

\usetheme{Warsaw}
\setbeamercovered{transparent}
\setbeamertemplate{footline}[frame number]

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{bbding}
\usepackage{tikz}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

\title[Dynamic MultiMesh Refinement] % (optional, use only with long paper titles)
{Dynamic Adaptive Multimesh Refinement for Coupled Physics Equations Applicable to Nuclear Engineering}

\author
{Kevin Dugan}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Texas A\&M University] % (optional, but mostly needed)
{
  Department of Nuclear Engineering\\
  Texas A\&M University
}

\date[May 2013] % (optional, should be abbreviation of conference name)
{Thesis Defense, 2013}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

\subject{Nuclear Engineering}
% This is only inserted into the PDF information catalog. Can be left
% out. 



% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}



% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

%\beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Introduction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Introduction}
\subsection*{Multiphysics Modeling}
\begin{frame}{Multiphysics Modeling}
\begin{itemize}
  \item Nuclear Engineers need to understand how separate physical processes interact with one another
\vskip1cm
  \item Modeling must incorporate coupled physical interactions
\vskip1cm
  \item Coupling produces numerical systems that are more difficult to solve accurately
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Adaptive Mesh Refinement}
\begin{frame}{Adaptive Mesh Refinement (AMR)}
\begin{itemize}
	\item AMR allows for accurate solutions with smaller systems
\vskip1cm
	\item Multimesh AMR allows optimized mesh for each solution component [useful in multiphysics]
\vskip1cm
  \item Multimesh Examples [T \& $\sigma$] and [T \& $\phi$]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Dynamic Mesh Refinement}
\begin{frame}{Dynamic Mesh Refinement}
\begin{itemize}
  \item Previous Work
  \begin{itemize}
	  \item Time dependent solutions require spatial meshes to change in time
	  \item Previous Publications [Solin, Dubcova, Lebrun-Grandi\'e] use costly approach
  \end{itemize}
\vskip1cm
  \item Extension proposed in this Master's Thesis work
  \begin{itemize}
    \item Propagating Mesh - Balances accurate solution \& fast solution time
  \end{itemize}
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Outline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Outline}
\begin{small}
\begin{multicols}{2}
  \tableofcontents
\end{multicols}
\end{small}
\end{frame}

\AtBeginSection[]
{
  \begin{frame}{Outline}
  \begin{small}
  \begin{multicols}{2}
    \tableofcontents[currentsection]
  \end{multicols}
  \end{small}
  \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Coupled Physics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multiphysics Modeling}
\subsection{Coupling}

\begin{frame}{Operator Split}
\begin{itemize}
  \setbeamercolor{itemize item}{fg=red}
	\item[\XSolidBrush] May not resolve coupling well
	\item[\XSolidBrush] Requires iteration between models
  \setbeamercolor{itemize item}{fg=green}
	\item[\Checkmark] Solves smaller problems
\end{itemize}
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/OperatorSplit.png}
\end{center}
\end{frame}

\begin{frame}{Monolithic (Our Choice)}
\begin{itemize}
  \setbeamercolor{itemize item}{fg=green}
	\item[\Checkmark] Fully resolves coupling
	\item[\Checkmark] No need to iterate between physics models
  \setbeamercolor{itemize item}{fg=red}
	\item[\XSolidBrush] Requires solution to large system
\end{itemize}
\begin{equation}
\frac{\partial}{\partial t}
\begin{bmatrix}
\phi \\
T \\
\end{bmatrix}
=
\begin{bmatrix}
L^{\phi \phi} && L^{\phi T} \\
L^{T \phi} && L^{T T} \\
\end{bmatrix}
\begin{bmatrix}
\phi \\
T \\
\end{bmatrix}
+
\begin{bmatrix}
Q_\phi \\
Q_T \\
\end{bmatrix} \nonumber
\end{equation}

\pause
  \begin{table}
  \begin{tabular}{l l}
  $L^{\phi\phi} = \nabla \!\cdot\! D \nabla - \Sigma_a(T)$ & $L^{\phi T} = 0$ \\
  $L^{T \phi} = \kappa$ & $L^{T T} = \nabla \!\cdot\! k \nabla$ \\
  \end{tabular}
  \end{table}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Mesh Refinement}

\begin{frame}{Adaptive Mesh Refinement}
\begin{itemize}
  \item Uses error indicator to locate which cells contribute greatest to solution error
\vskip0.5cm
  \item Refines mesh only in locations where error is large [keeps system small]
\vskip0.5cm
  \item Solution has same (or smaller) numerical error as uniform refinement but smaller system
\vskip0.5cm
  \item Produces meshes optimized to solution's properties
\end{itemize}
\end{frame}

\begin{frame}{Multimesh AMR}
\begin{itemize}
  \item Produces optimized meshes for each solution component in a multiphysics simulation
\vskip1cm
  \item Mesh for each solution component is free to conform to the needs of a specific solution component
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamic Mesh Refinement}
\begin{frame}{Adaptivity at Every Time Step}
\begin{itemize}
  \item Spatial mesh may change in time
\vskip1cm
  \item Previous publications start each new time step refinement on coarse mesh
\vskip1cm
  \item Modifications so that new time step starts refinement on previous time step mesh
\end{itemize}
\end{frame}

\begin{frame}{Propagating Mesh I}
Seek an extension to dynamic AMR algorithm presented in previous publications by:
\begin{itemize}
  \item Using an adaptively refined mesh for multiple time steps
  \item Spatially adapting only when solution has changed significantly
\end{itemize}
\pause
To extend in this direction, need to:
\begin{enumerate}
  \setbeamercolor{item projected}{bg=red,fg=black}
  \item Quantify how a solution changes in time
  \item Determine when to spatially adapt
  \item Determine where to spatially adapt
\end{enumerate}
\end{frame}

\begin{frame}{Propagating Mesh II}
\begin{center}
\includegraphics[width=\textwidth]{figures/timeDomain.png}
\end{center}
\begin{itemize}
  \item Mesh is spatially adapted at $t_1$
  \item ``$i$'' denotes current time step
  \item Same spatial mesh used until $t_{1+\alpha}$
  \item $\alpha$ not known \emph{a priori}
\end{itemize}
\end{frame}

\begin{frame}{Propagating Mesh III}
\begin{equation}
\theta_i = \cos^{-1}\left( \frac{V_1 \cdot V_i}{\|V_1\|\|V_i\|} \right) \leq \theta_\mathrm{tol} \nonumber
\end{equation}
\begin{itemize}
  \item $\theta_\mathrm{tol}$ determined by user [1$^\circ$ - 45$^\circ$]
  \item $\alpha$ determined from when $\theta_i > \theta_\mathrm{tol}$
\end{itemize}
\begin{enumerate}
  \setbeamercolor{item projected}{bg=red,fg=black}
  \item Quantifies how a solution behaves in time
  \item Specifies when to spatially adapt
\end{enumerate}
\end{frame}

\begin{frame}{Propagating Mesh IV}
\begin{equation}
\overline{V} = \frac{1}{1+\alpha} \sum_{i=1}^{1+\alpha} V_i \nonumber
\end{equation}
\begin{itemize}
  \item Each \{$V_i$\} is stored in time interval [$t_1,t_{1+\alpha}$]
  \item Vectors are averaged when $\theta_i > \theta_\mathrm{tol}$
  \item Averaging captures solution behavior within time interval
\end{itemize}
\begin{enumerate}
  \setbeamercolor{item projected}{bg=red,fg=black}
  \setcounter{enumi}{2}
  \item Specifies where to spatially adapt
\end{enumerate}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Solution Techniques
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solution Techniques}
\subsection{Newton's Method}

\begin{frame}{Nonlinear Solve}
Iteration Sequence:
\begin{equation}
J(\vec{U}^l) \delta \vec{U} = -F(\vec{U}^l), \nonumber
\end{equation}
\begin{equation}
\vec{U}^{l+1} = \vec{U}^l + \delta \vec{U} \nonumber
\end{equation}
\pause
Nonlinear Residual and Jacobian Formulation:
\begin{equation}
F(\vec{U}) = 
\begin{bmatrix}
F_\phi \\
F_T \\
\end{bmatrix} \qquad
J(\vec{U}) = 
\begin{bmatrix}
\frac{\partial F_\phi}{\partial \phi} & \frac{\partial F_\phi}{\partial T} \\
\frac{\partial F_T}{\partial \phi} & \frac{\partial F_T}{\partial T} \\
\end{bmatrix} \nonumber
\end{equation}
\pause
Linear Solver:
\begin{itemize}
  \setbeamercolor{itemize item}{fg=green}
  \item[\Checkmark] Direct Solver [UMFPACK]
  \setbeamercolor{itemize item}{fg=red}
  \item[\XSolidBrush] Iterative Solver [GMRes]
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Runge-Kutta}

\begin{frame}{Runge-Kutta General Formulation}
Stage Residual Formulation for $\frac{\partial \vec{U}}{\partial t} = f(\vec{U},t)$:
\begin{equation}
F(Y_i) = Y_i - \vec{U}^n + \tau \sum_{j=1}^i a_{ij} f(Y_j,t_n+c_j\tau) \qquad i=1,2,...,s \nonumber
\end{equation}
\pause
Solution Increment:
\begin{equation}
\vec{U}^{n+1} = \vec{U}^n + \tau \sum_{i=1}^s b_i f(Y_i, t_n+c_i\tau) \nonumber
\end{equation}
\pause
General Butcher Tableau Form:
\begin{center}
\begin{tabular}{c|c}
$c$    &     $A$ \\ \hline
           &     $b^T$ \\
\end{tabular}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Finite Element Method}

\begin{frame}{Continuous Galerkin FEM}
\begin{enumerate}
  \item Transform continuous equations into weakform [$\int_\Omega b_i ( )$]
  \item Approximate solution by linear combination of basis functions
  \item Evaluate integrals over domain by sums of integrals over cells, e.g.,
\end{enumerate}
\begin{equation}
\int_\Omega b_i b_j \rightarrow \sum_{K\in\mathcal{T}} \int_K b_i b_j \nonumber
\end{equation}
\begin{enumerate}
  \setcounter{enumi}{3}
  \item Produce mass and stiffness matrices
  \item Numerical Quadrature
\end{enumerate}
\end{frame}

\begin{frame}{Assembling Coupling Terms on Independent Meshes I}
\begin{itemize}
  \item Steady state residual contains product of basis functions from possibly independent meshes $[M^{T \phi}_\kappa]$
  \item Can no longer perform transformation from integral over domain to sum over cells
\end{itemize}
\begin{equation}
\int_\Omega b_i^T \kappa b_j^\phi \nrightarrow \sum_{K\in\mathcal{T}} \int_K b_i^T \kappa b_j^\phi \nonumber
\end{equation}
\pause
Saving Assumptions:
\begin{small}
\begin{enumerate}
  \item Independent meshes are derived from common coarse mesh
  \item Refinement by regular bisection of cells
  \item Embedded finite element spaces - basis functions can be represented as LC of basis functions on child cells
\end{enumerate}
\end{small}
\end{frame}

\begin{frame}{Assembling Coupling Terms on Independent Meshes II}
\begin{itemize}
  \item Assumption 1 \& 2 imply that coupling terms can be assembled on an intersection mesh $[\mathcal{T}_\phi \cap \mathcal{T}_T]$
\end{itemize}
\begin{equation}
\int_\Omega b_i^T b_j^\phi \rightarrow \sum_{K\in\mathcal{T}_\phi \cap \mathcal{T}_T} \int_K b_i^T b_j^\phi \nonumber
\end{equation}
\pause
\begin{itemize}
  \item Assumption 3 implies that the basis functions on a parent cell can be represented in terms of child cells
\end{itemize}
\vskip0.2cm
\begin{equation}
b_i^\phi |_{K_c} = B_c^{il} b_l^T |_{K_c} \nonumber
\end{equation}
\vskip-0.2cm
\begin{itemize}
  \item The matrix $B_c^{il}$ interpolates data from a parent cell to its $c^{th}$ child
\end{itemize}
\end{frame}

\addtocontents{toc}{\newpage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Code Verification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Code Verification}
\begin{frame}{Method of Manufactured Solutions}
\begin{itemize}
  \item Choose exact solution
\vskip0.5cm
  \item Insert into undiscretized partial differential equation
\vskip0.5cm
  \item Obtain a residual that is used as a source term
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Spatial Discretization}
\begin{frame}{Manufactured Solution - Space}
\begin{equation}
U(\vec{x}) = C_u \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L} \right)^2  \right] e^{-\frac{\left(x_i - x_i^{0,u}\right)^2}{\sigma_u}} \nonumber
\end{equation}
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/MMS-Solution-Stationary-Gaussian.png}
    \end{center}
  \column{0.6\textwidth}
    \begin{itemize}
      \item Formulation for one of the two solution components
      \item $x^{0,u}$ is not time dependent
      \item Gaussian does not have finite polynomial representation
      \item $\Omega = \left[-L,L\right]^d$
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Convergence Analysis}
\begin{equation}
  \only<1>{
  \varepsilon \propto h^{p+1} \nonumber
  } \only<2>{
  \ln \varepsilon \propto (p+1) \ln h \nonumber
  }
\end{equation}
\vskip-0.3cm
\begin{columns}
  \column{0.5\textwidth}
    \begin{center}
    \begin{tiny}
    Neutron Flux ($\phi$)
    \end{tiny}
    \includegraphics[width=\textwidth]{figures/Phi1_global_h.png}
    \end{center}
  \column{0.5\textwidth}
    \begin{center}
    \begin{tiny}
    Material Temperature (T)
    \end{tiny}
    \includegraphics[width=\textwidth]{figures/Phi2_global_h.png}
    \end{center}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Temporal Discretization}
\begin{frame}{Manufactured Solution - Time}
\begin{equation}
U(\vec{x},t) = C_u \left[ \sin(\omega_ut) + 1 \right] \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L} \right)^2  \right] \nonumber
\end{equation}
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/MMS-Solution-Quadratic.png}
    \end{center}
  \column{0.6\textwidth}
    \begin{itemize}
      \item Amplitude changes in time
      \item Q2 elements used for spatial component
      \item Time dependence does not have finite polynomial representation
      \item $\Omega = \left[-L,L\right]^d$
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Convergence Analysis}
\vskip-0.5cm
\begin{table}[H]
  \centering
  \begin{tiny}
  \begin{tabular}{|c|c|}
  \hline
  Backward Euler (BE) & $\mathcal{O}(\tau)$ \\ \hline
  Crank-Nicholson (CN) & $\mathcal{O}(\tau^2)$ \\ \hline
  SDIRK22 & $\mathcal{O}(\tau^2)$ \\ \hline
  SDIRK33 & $\mathcal{O}(\tau^3)$ \\ \hline
  \end{tabular}
  \end{tiny}
\end{table}
\vskip-0.5cm
\begin{columns}
  \column{0.5\textwidth}
    \begin{center}
    \begin{tiny}
    Neutron Flux ($\phi$)
    \end{tiny}
    \includegraphics[width=\textwidth]{figures/Phi1_time.png}
    \end{center}
  \column{0.5\textwidth}
    \begin{center}
    \begin{tiny}
    Material Temperature (T)
    \end{tiny}
    \includegraphics[width=\textwidth]{figures/Phi2_time.png}
    \end{center}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamic AMR}
\begin{frame}{Test Setup}
Testing Propagating Mesh feature:
\begin{itemize}
  \item SDIRK33, 128 time steps $t\in[0,1s]$
  \item Uniformly refined static mesh
  \item $\theta_\mathrm{tol}$ at 1, 10, 45
  \item Use two manufactured solutions [Traveling Gaussian, Reactor]
\end{itemize}
\end{frame}

\begin{frame}{Manufactured Solution - Traveling Gaussian}
\begin{equation}
U(\vec{x}) = C_u \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L} \right)^2  \right] e^{-\frac{\left(x_i - x_i^{0,u}\right)^2}{\sigma_u}} \nonumber
\end{equation}
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/MMS-Solution-Stationary-Gaussian.png}
    \end{center}
  \column{0.6\textwidth}
    \begin{equation}
    x^{0,u}(t) = \begin{bmatrix} \cos(\omega_u t)\\ \sin(\omega_u t)\\ 0 \end{bmatrix} \nonumber
    \end{equation}
\end{columns}
\end{frame}

\begin{frame}{Traveling Gaussian}
\only<1|handout:1>{
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/movingGaussian01.png}};
      \draw<1>[black,thick] (0.04,2.17) rectangle (4.29,2.17);
    \end{tikzpicture}
    \begin{tiny}
    t = 0s 
    \end{tiny}
    \end{center}
  \column{0.4\textwidth}
    \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{figures/movingGaussian02.png}};
      \draw<1>[black,thick] (0.04,2.17) rectangle (4.29,2.17);
    \end{tikzpicture}
    \begin{tiny}
    t = 0.067s 
    \end{tiny}
    \end{center}
\end{columns}
}
\only<2|handout:2>{
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/movingGaussian03.png}
    \begin{tiny}
    t = 0.5s 
    \end{tiny}
    \end{center}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/movingGaussian04.png}
    \begin{tiny}
    t = 1.33s 
    \end{tiny}
    \end{center}
\end{columns}
}
\begin{enumerate}
  \item<1|handout:1> Solution moves but mesh remains static over a small time interval
  \item<2|handout:2> As solution moves, mesh follows
\end{enumerate}
\end{frame}

\begin{frame}{Convergence Analysis - \only<1|handout:1>{DoFs} \only<2|handout:2>{CPU Time}}
\begin{columns}
  \column{0.5\textwidth}
    \begin{center}
      \begin{tiny}
      Neutron Flux ($\phi$)
      \end{tiny}
    \includegraphics<1|handout:1>[width=\textwidth]{figures/travelingGauss1.png}
    \includegraphics<2|handout:2>[width=\textwidth]{figures/travelingGauss1_time.png}
    \end{center}
  \column{0.5\textwidth}
    \begin{center}
      \begin{tiny}
      Material Temperature (T)
      \end{tiny}
    \includegraphics<1|handout:1>[width=\textwidth]{figures/travelingGauss2.png}
    \includegraphics<2|handout:2>[width=\textwidth]{figures/travelingGauss2_time.png}
    \end{center}
\end{columns}
\end{frame}

\begin{frame}{Manufactured Solution - Reactor}
\begin{equation}
U(\vec{x},t) = C_u \left[ \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L}  \right)^2 \right] + \frac{\alpha}{\sigma} t e^{-\alpha t} \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L}  \right)^2 \right] e^{- \frac{\left( x_i - x_i^0 \right)^2}{\sigma} } \right] \nonumber
\end{equation}
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics<1|handout:1>[width=\textwidth]{figures/MMS-Solution-Quadratic.png}
    \includegraphics<2|handout:2>[width=\textwidth]{figures/MMS-Solution-Maxwellian.png}
    \includegraphics<3|handout:3>[width=\textwidth]{figures/Maxwellian-noYaxis.png}
    \end{center}
  \column{0.6\textwidth}
    \begin{itemize}
      \item Smooth initial condition
      \item Gaussian peak appears and disappears
      \item Mimics solution from a reactor accident
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Reactor}
\only<1|handout:1>{
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/maxwellian01.png}
    \begin{tiny}
    t = 0s 
    \end{tiny}
    \end{center}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/maxwellian02.png}
    \begin{tiny}
    t = 0.047s 
    \end{tiny}
    \end{center}
\end{columns}
}
\only<2|handout:2>{
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/maxwellian03.png}
    \begin{tiny}
    t = 0.094s 
    \end{tiny}
    \end{center}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/maxwellian04.png}
    \begin{tiny}
    t = 0.914s 
    \end{tiny}
    \end{center}
\end{columns}
}
\begin{enumerate}
  \item<1|handout:1> Initial mesh does not anticipate peak
  \item<2|handout:2> Refinement concentrates around peak
\end{enumerate}
\end{frame}

\begin{frame}{Convergence Analysis - \only<1|handout:1>{DoFs} \only<2|handout:2>{CPU Time}}
\begin{columns}
  \column{0.5\textwidth}
    \begin{center}
      \begin{tiny}
      Neutron Flux ($\phi$)
      \end{tiny}
    \includegraphics<1|handout:1>[width=\textwidth]{figures/propagatingMeshMaxwellian1.png}
    \includegraphics<2|handout:2>[width=\textwidth]{figures/propagatingMeshMaxwellian1_time.png}
    \end{center}
  \column{0.5\textwidth}
    \begin{center}
      \begin{tiny}
      Material Temperature (T)
      \end{tiny}
    \includegraphics<1|handout:1>[width=\textwidth]{figures/propagatingMeshMaxwellian2.png}
    \includegraphics<2|handout:2>[width=\textwidth]{figures/propagatingMeshMaxwellian2_time.png}
    \end{center}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Conclusions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}
\begin{frame}{Applicability}
Dynamic AMR:
\begin{itemize}
  \item Numerical system $\sim10\times$ smaller than uniform static mesh
  \item Provides faster wall-clock times, in general
  \item Easily extended to compute shock front problems
\end{itemize}
\pause
Propagating Mesh Feature:
\begin{itemize}
  \item Lightweight version of previous algorithms
  \item Not dependent on initial condition
  \item Appropriate method for problems studied in Nuclear Engineering
\end{itemize}
\end{frame}

\begin{frame}{Future Work}
Jacobian-Free Newton Krylov (JFNK):
\begin{itemize}
  \item Larger system size $\Rightarrow$ No direct solver $\Rightarrow$ GMRes
  \item Jacobian approximation performed with vectors from Krylov subspace
  \item Only residual is required to be stored
\end{itemize}\pause
Restarting Adaptivity:
\begin{itemize}
  \item Use trial solution to test how solution will change
  \item Preliminary testing showed an increase in accuracy, but not drastic
\end{itemize}
\end{frame}

\begin{frame}{Questions}
\begin{center}
Thank You! \\
Questions?
\end{center}
\end{frame}




\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Extra Slides
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\backupbegin
\setbeamercolor{background canvas}{bg=black}
\begin{frame}[plain]{}
\end{frame}
\setbeamercolor{background canvas}{bg=white}

\begin{frame}{Manufactured Solution - Spacetime}
\begin{equation}
U(\vec{x},t) = C_u \left[ \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L}  \right)^2 \right] + \frac{\alpha}{\sigma} t e^{-\alpha t} \prod_{i=1}^d \left[ 1 - \left( \frac{x_i}{L}  \right)^2 \right] e^{- \frac{\left( x_i - x_i^0 \right)^2}{\sigma} } \right] \nonumber
\end{equation}
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics<1|handout:1>[width=\textwidth]{figures/MMS-Solution-Maxwellian.png}
  \includegraphics<2|handout:2>[width=\textwidth]{figures/Maxwellian-noYaxis.png}
    \end{center}
  \column{0.6\textwidth}
    \begin{itemize}
      \item Smooth initial condition
      \item Gaussian peak appears and disappears
      \item Space and Time do not have finite polynomial representations
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}{Connection between Space and Time}
\vskip-0.45cm
\begin{columns}
  \column{0.5\textwidth}
    \begin{center}
    \includegraphics<1|handout:0>[width=\textwidth]{figures/Space_BE_Q2-noCirc.png}
    \includegraphics<2|handout:0>[width=0.5\textwidth]{figures/BlankConvergence.png}
    \end{center}
  \column{0.5\textwidth}
    \begin{center}
    \includegraphics<1|handout:0>[width=\textwidth]{figures/Time_BE_Q2-noCirc.png}
    \includegraphics<2|handout:0>[width=0.5\textwidth]{figures/BlankConvergence.png}
    \end{center}
\end{columns}
\vskip-0.3cm
\begin{columns}
  \column{0.5\textwidth}
    \begin{center}
    \includegraphics<1|handout:0>[width=\textwidth]{figures/BlankConvergence.png}
    \includegraphics<2|handout:0>[width=\textwidth]{figures/Space_SDIRK33_Q2-noCirc.png}
    \end{center}
  \column{0.5\textwidth}
    \begin{center}
    \includegraphics<1|handout:0>[width=\textwidth]{figures/BlankConvergence.png}
    \includegraphics<2|handout:0>[width=\textwidth]{figures/Time_SDIRK33_Q2-noCirc.png}
    \end{center}
\end{columns}
\end{frame}

\begin{frame}{Connection between Space and Time}
\vskip-0.45cm
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/Space_BE_Q2.png}
    \end{center}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/Time_BE_Q2.png}
    \end{center}
\end{columns}
\vskip-0.3cm
\begin{columns}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/Space_SDIRK33_Q2.png}
    \end{center}
  \column{0.4\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{figures/Time_SDIRK33_Q2.png}
    \end{center}
\end{columns}
\end{frame}


\backupend

\end{document}
